#include "ClaseTrack.h"
using namespace cv;
using namespace std;

public class ClaseTrack {
public:
	void Detectar();
	ClaseTrack() {
		Arranque();

	}
	void Arranque();
private:

protected:

	

};

void ClaseTrack::Arranque() {

}
void ClaseTrack::Detectar(void)
{
	cv::Mat edges, frame, src, HSV_image, LAB_image, HSV_image_local, combinado;
	cv::Scalar hsvlow(100, 107, 62), hsvhigh(200, 158, 113);
	cv::Scalar lablow(100, 107, 62), labhigh(200, 158, 113), colorD(0, 255, 0);
	//LOCAL:
	cv::Mat HSV_imageM, LAB_imageM;
	cv::Mat HSV_crop, LAB_crop;
	cv::Mat hue, hsv, hist;
	cv::Mat histimg = Mat::zeros(200, 320, CV_8UC3), backproj;
	cv::Rect2d rec;
	RotatedRect ELIP; // Tiene las coordenadas base para trackear.
					  //Mat frame;
	Rect trackWindow;
	bool backprojMode = false;
	int hsize = 16;

	float hranges[] = { 0,180 };
	const float* phranges = hranges;

	VideoCapture  cap(0); //capture the video from web cam

	cap.set(CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CAP_PROP_FRAME_HEIGHT, 240);

	radioButton1->Checked = true;
	cap.set(CAP_PROP_FPS, 30);
	if (!cap.isOpened())  // if not success, exit program
	{
		label2->Text = "Cannot open the webCam";
		return;
	}
	cap.read(frame);
	int64 totpixels = frame.rows * frame.cols;
	while (1)
	{

		// 1. Cambio de Resoluci�n.
		if (BanderaResolucion)
		{
			if (radioButton1->Checked)
			{
				cap.set(CAP_PROP_FRAME_WIDTH, 320);
				cap.set(CAP_PROP_FRAME_HEIGHT, 240);
				c_alto = 240;
				c_ancho = 320;
				cap.set(CAP_PROP_FPS, 30);
				BanderaResolucion = false;
			}
			else if (radioButton2->Checked)
			{
				cap.set(CAP_PROP_FRAME_WIDTH, 640);
				cap.set(CAP_PROP_FRAME_HEIGHT, 480);
				cap.set(CAP_PROP_FPS, 30);
				c_alto = 480;
				c_ancho = 640;
				BanderaResolucion = false;
			}
		}

		// 1.1 Lectura del Frame.
		double tic = (double)cvGetTickCount();
		bool bSuccess = cap.read(frame); // read a new frame from video
		if (!bSuccess)
		{
			label2->Text = "Cannot read a frame from video stream";
			break;
		}

		// 2. Vertical y Horizontal FLIP
		if (FLIPMOD)
		{
			if (checkBox2->Checked && checkBox3->Checked)
			{
				varFlip = -1;
				Flipar = true;
			}

			if (checkBox2->Checked && (!checkBox3->Checked))
			{
				varFlip = 0;
				Flipar = true;
			}

			if ((!checkBox2->Checked) && checkBox3->Checked)
			{
				varFlip = 1;
				Flipar = true;
			}

			if ((!checkBox2->Checked) && (!checkBox3->Checked))
			{
				varFlip = 0;
				Flipar = false;
			}


			FLIPMOD = false;
		}

		if (Flipar)
			flip(frame, frame, varFlip);

		//3. Conversion a Espacios y Crear "Combinado" -> Img con el obj filtrado.
		cvtColor(frame, HSV_image, cv::COLOR_BGR2HSV); //BGR to HSV
		cvtColor(frame, LAB_image, cv::COLOR_BGR2Lab); //to LAB.
		inRange(HSV_image, hsvlow, hsvhigh, HSV_imageM);
		inRange(LAB_image, lablow, labhigh, LAB_imageM);
		Mat* pMats[2];
		pMats[0] = &HSV_imageM;
		pMats[1] = &LAB_imageM;
		for (int x = 0; x < 2; x++) {
			erode(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(7, 7)));
			dilate(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
		}

		for (int x = 0; x < 2; x++) {
			dilate(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
			erode(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
		}
		bitwise_and(*pMats[0], *pMats[1], combinado);

		// Seguir el objeto de forma natural
		if (trackObject) {
			int ch[] = { 0, 0 };
			hue.create(HSV_image.size(), HSV_image.depth()); // matriz similar a HSV.
			mixChannels(&HSV_image, 1, &hue, 1, ch, 1); // llenar HUE

														// Seguir a un NUEVO objeto.
			if (trackObject < 0)
			{
				Mat roi(hue, rec), maskroi(combinado, rec); // solo regiones de interes en color y en BIN.
				calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges); // hitograma de HUE
				normalize(hist, hist, 0, 255, NORM_MINMAX); // normalizar.

				trackWindow = rec; // es un Rect.
				trackObject = 1;

				histimg = Scalar::all(0);
				int binW = histimg.cols / hsize;
				Mat buf(1, hsize, CV_8UC3);
				for (int i = 0; i < hsize; i++)
					buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180. / hsize), 255, 255);

				cvtColor(buf, buf, COLOR_HSV2BGR);

				for (int i = 0; i < hsize; i++)
				{
					int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows / 255);
					rectangle(histimg, cv::Point(i*binW, histimg.rows),
						cv::Point((i + 1)*binW, histimg.rows - val),
						Scalar(buf.at<Vec3b>(i)), -1, 8);
				}
			}


			// Dibujar Objetos Encontrados
			Point2f centroE;
			//float radioE;
			Mat auxiliarTMP = combinado.clone();
			vector<vector<cv::Point> > contornos;
			vector<Vec4i> hierarchy;
			findContours(auxiliarTMP, contornos, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			//findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
			if (contornos.size() > 0) {
				//contornos.max_size()
				for (int i = 0; i < contornos.size(); i++) {
					// Suele dibujarse
					//drawContours(frame, contornos, i, Scalar(0, 255, 255), 2);
				}
				//minEnclosingCircle(contornos, centroE, radioE);
				//if (radioE > 10) {
				//	circle(frame,centroE,(int) radioE,Scalar(255,0,255),3,8,0);
				//}
			}
			//findContours(auxiliarTMP, contornos, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
			//findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
			calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
			backproj &= combinado; // muy importante!!
			int64 npixels = countNonZero(backproj);
			double porcentaje = ((double)npixels / (double)totpixels);
			if (porcentaje > 0.0001) {
				RotatedRect trackBox = CamShift(backproj, trackWindow, TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1));
				ellipse(frame, trackBox, Scalar(255, 128, 0), 3, LINE_AA);
				rectangle(frame, trackBox.boundingRect(), Scalar(0, 224, 64), 3, LINE_AA);
				X_axis = trackBox.center.x;
				Y_axis = trackBox.center.y;
				Z_axis = trackBox.size.height;
			}
			else {
				label2->Text = "he perdido momentaneamente el objeto!";
				X_axis = 0;
				Y_axis = 0;
				Z_axis = 0;
			}

		}
		/*
		if (backprojMode)
		cvtColor(backproj, combinado, COLOR_GRAY2BGR); // muestra lo que descubri�.
		*/

		// Calculo de FPS:
		double toc = (cvGetTickCount() - tic) / cvGetTickFrequency();
		toc = toc / 1000000;
		float fps = (float)(1 / toc);
		//imshow("Thresholded Image", combinado); //show the thresholded image
		// imshow("edges", frame);


		if (Seguimiento) {
			// rec = selectROI("edges", frame);
			rec = ELIP.boundingRect();
			if (!(rec.width == 0 || rec.height == 0))
			{
				vector<Mat> canalesHSV(3);
				vector<Mat> canalesLAB(3);

				Mat LAB_l, LAB_a, LAB_b, HSV_h, HSV_s, HSV_v;

				Mat subW = frame.clone();

				subW = subW(rec);
				cvtColor(subW, HSV_crop, CV_BGR2HSV);
				cvtColor(subW, LAB_crop, CV_BGR2Lab);

				double mL, xL, mA, xA, mB, xB, mH, xH, mS, xS, mV, xV;
				split(HSV_crop, canalesHSV);
				split(LAB_crop, canalesLAB);
				minMaxLoc(canalesLAB[0], &mL, &xL);
				minMaxLoc(canalesLAB[1], &mA, &xA);
				minMaxLoc(canalesLAB[2], &mB, &xB);
				minMaxLoc(canalesHSV[0], &mH, &xH);
				minMaxLoc(canalesHSV[1], &mS, &xS);
				minMaxLoc(canalesHSV[2], &mV, &xV);
				if (mS > 20) {
					hsvlow = Scalar(mH, mS, mV);
				}
				else {
					hsvlow = Scalar(mH, 20, mV);
				}
				if (xV < 230) {
					hsvhigh = Scalar(xH, xS, xV);
				}
				else {
					hsvhigh = Scalar(xH, xS, 230);
				}

				lablow = Scalar(mL, mA, mB);
				labhigh = Scalar(xL, xA, xB);

				trackObject = -1;
				Seguimiento = false;
			}

		}
		else if (Ban_Deteccion) {
			src = frame.clone();
			Mat src_gray;
			Mat src_hsv;
			Mat out_hsv_low;
			Mat out_hsv_upp;
			Mat out_hsv_Weight;
			Mat dst_rgb;
			Mat src_gray2;
			Mat sharped;
			Mat imgTmp;
			/// Convert it to gray
			cvtColor(src, src_gray, CV_BGR2GRAY);
			cvtColor(src, src_hsv, CV_BGR2HSV);

			Mat ch1, ch2, ch3;
			vector<Mat> channels(3);
			split(src, channels);
			ch1 = channels[0];
			ch2 = channels[1];
			ch3 = channels[2];

			Mat hueX;
			int chX[] = { 0, 0 };
			int chC[] = { 0,0,1,1,1,2 };



			switch (bola) {
			case 1:
				// ROJO
				inRange(src_hsv, Scalar(0, 100, 0), Scalar(15, 255, 220), out_hsv_low);
				inRange(src_hsv, Scalar(160, 100, 0), Scalar(179, 255, 220), out_hsv_upp);
				addWeighted(out_hsv_low, 1.0, out_hsv_upp, 1.0, 0.0, out_hsv_Weight, -1);
				break;
			case 2:
				// NARANJA-AMARILLO
				inRange(src_hsv, Scalar(16, 100, 0), Scalar(35, 255, 255), out_hsv_Weight);
				break;
			case 3:
				// VERDE
				inRange(src_hsv, Scalar(35, 60, 0), Scalar(85, 255, 220), out_hsv_Weight);
				break;
			case 4:
				//CYAN
				inRange(src_hsv, Scalar(86, 100, 0), Scalar(105, 255, 255), out_hsv_Weight);
				break;
			case 5:
				//AZUL
				inRange(src_hsv, Scalar(106, 100, 0), Scalar(135, 255, 255), out_hsv_Weight);
				break;
			case 6:
				//VIOLETA
				inRange(src_hsv, Scalar(136, 100, 0), Scalar(159, 255, 255), out_hsv_Weight);
				break;
			default:
				cout << "Numero no v�lido" << endl;
				break;
			}


			GaussianBlur(src_gray, imgTmp, cv::Size(0, 0), 3);
			addWeighted(src_gray, 1.5, imgTmp, -0.5, 0, sharped);
			//imshow("otra", out_hsv_Weight);
			//imshow("ColorEspacio", sharped);

			Mat auxTMP = out_hsv_Weight.clone();
			vector<vector<cv::Point> > contornos;
			findContours(auxTMP, contornos, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			if (contornos.size() > 0) {
				//contornos.max_size()
				for (int i = 0; i < contornos.size(); i++) {
					double area0 = contourArea(contornos[i]);
					if (area0 > 100.0) {
						//cout << "Area de Objeto " << i << " es: " << (double)area0 << endl;
						drawContours(src, contornos, i, Scalar(0, 255, 255), 2);
						ELIP = fitEllipse(contornos[i]);

						ellipse(frame, ELIP, Scalar(100, 00, 240), 2, 7); // se peerder� la elipse.

																		  // SE DEBE APLICAR TOMA DE HSV para INCREMENTAR LA REGION.
					}
				}
			}
			Ban_Deteccion = false;
			Seguimiento = true;

		}
		else if (Verbose = true)
		{
			//const char C1 = "HSV";
			//const char C2 = "LAB";
			//label2->Text = "HSV: ";
			//label3->Text = "LAB: ";

			for (int loop = 0; loop < 1; loop++)
			{
				//label2->Text = System::String::Concat(hsvlow[loop].ToString(), "-", hsvhigh[loop].ToString(), ", ");
				//label3->Text = System::String::Concat(lablow[loop].ToString(), "-", labhigh[loop].ToString(), ", ");
			}

			Verbose = false;
		}
		// Siempre se muestra FPS
		label1->Text = System::String::Concat("FPS: ", fps.ToString());
		XX = (X_axis / c_ancho) * 255;
		YY = (Y_axis / c_alto) * 255;
		ZZ = (Z_axis / c_alto) * 255;
		label4->Text = System::String::Concat("Pos X: ", XX.ToString());
		label5->Text = System::String::Concat("Pos Y: ", YY.ToString());
		label6->Text = System::String::Concat("Pos Z: ", ZZ.ToString());

		DrawCVImage(this->panel1, frame.clone());
	}
	cap.release();
	return;

}