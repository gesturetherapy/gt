#pragma once
/*
#include <windows.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/tracking.hpp>
#include <opencv/cv.h>
#include <opencv\cv.hpp>
#include <opencv/highgui.h>
*/
//#include <stdafx.h>

using namespace System;
using namespace System::Threading;

ref class Traquer
{
private:
	int sleepTime;
	static Random^ random = gcnew Random();

	// constructor to initialize a MessagePrinter object
public:
	Traquer()
	{
		// pick random sleep time between 0 and 5 seconds
		sleepTime = random->Next(5001);
	}

	//controls Thread that prints message
	void Detectar()
	{
		// obtain reference to currently executing thread
		Thread^ current = Thread::CurrentThread;
		
		// put thread to sleep for sleepTime amount of time
		Console::WriteLine(current->Name + " going to sleep for " + sleepTime);

		Thread::Sleep(sleepTime);

		// print thread name
		Console::WriteLine(current->Name + " done sleeping");

	} // end method Detectar


}; // end class MessagePrinter  

int main()
{
	// Create and name each thread. Use MessagePrinter's
	// Print method as argument to ThreadStart delegate.
	Traquer^ printer1 = gcnew Traquer();
	Thread^ thread1 = gcnew Thread(gcnew ThreadStart(printer1, &Traquer::Detectar));
	thread1->Name = "thread1";

	Traquer^ printer2 = gcnew Traquer();
	Thread^ thread2 = gcnew Thread(gcnew ThreadStart(printer2, &Traquer::Detectar));
	thread2->Name = "thread2";
	

	Traquer^ printer3 = gcnew Traquer();
	Thread^ thread3 = gcnew Thread(gcnew ThreadStart(printer3, &Traquer::Detectar));
	thread3->Name = "thread3";

	Console::WriteLine("Starting threads");
	// call each thread's Start method to place each
	// thread in Started state
	thread1->Start();
	thread2->Start();
	thread3->Start();

	Console::WriteLine("Threads started\n");
	Console::ReadLine();

	return 0;
}