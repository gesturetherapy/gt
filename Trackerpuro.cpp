#include "Trackerpuro.h"


// Constructor
Trackerpuro::Trackerpuro()
{
	strMensajes = new std::string;
	//strCola = new std::string;
	frame = new cv::Mat; //verificar su integridad.
	frameDRAW = new cv::Mat;
	Banderita = 0;
	FlipHead = new bool;
	FlipMirror = new bool;
	*FlipHead = false;
	*FlipMirror = true;
	FLIPMOD = true;
	trackObject = 0;
	bola = 3;
}

Trackerpuro::~Trackerpuro()
{
	delete strMensajes;
	//delete strCola;
	delete frame;
	delete frameDRAW;
	delete FlipHead;
	delete FlipMirror;
}

int Trackerpuro::ExecDetect(bool Valor)
{
	Ban_Deteccion = Valor;
	return 1;
}

int Trackerpuro::Fun2Mirror(bool Valor) 
{
	FLIPMOD = true;
	*FlipMirror = Valor;
	return 1;
}

int Trackerpuro::Fun2Invert(bool Valor) {
	FLIPMOD = true;
	*FlipHead = Valor;
	return 1;
}

void Trackerpuro::Detectar(void)
{
	using namespace cv;
	using namespace std;
	Mat edges; // frame;
	Mat src, HSV_image, LAB_image, HSV_image_local, combinado;
	Scalar hsvlow(100, 107, 62), hsvhigh(200, 158, 113);
	Scalar lablow(100, 107, 62), labhigh(200, 158, 113), colorD(0, 255, 0);
	//LOCAL:
	Mat HSV_imageM, LAB_imageM;
	Mat HSV_crop, LAB_crop;
	Mat hue, hsv, hist;
	Mat histimg = Mat::zeros(200, 320, CV_8UC3), backproj;
	Rect2d rec;
	RotatedRect ELIP; // Tiene las coordenadas base para trackear.
	Rect trackWindow;
	int firstobject = 0;
	Rect2d MascaraELIP;
	bool backprojMode = false;
	int hsize = 16;
	bool SQ = false;
	float hranges[] = { 0,180 };
	const float* phranges = hranges;

	VideoCapture  cap(0); //capture the video from web cam
	cap.set(CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CAP_PROP_FRAME_HEIGHT, 240);

		cap.set(CAP_PROP_FPS, 30);
	if (!cap.isOpened())  // if not success, exit program
	{
		*strMensajes = std::string("Cannot open the webCam");
		return;
	}
	cap.read(*frame);
	*frameDRAW = frame->clone();
	int64 totpixels = frame->rows * frame->cols;
	while (1)
	{
		if (SALIR) {
			break;
		}

		// 1. Cambio de Resoluci�n.
		if (BanderaResolucion)
		{
			if (res320x240)
			{
				cap.set(CAP_PROP_FRAME_WIDTH, 320);
				cap.set(CAP_PROP_FRAME_HEIGHT, 240);
				c_alto = 240;
				c_ancho = 320;
				cap.set(CAP_PROP_FPS, 30);
				BanderaResolucion = false;
			}
			else if (res640x480)
			{
				cap.set(CAP_PROP_FRAME_WIDTH, 640);
				cap.set(CAP_PROP_FRAME_HEIGHT, 480);
				cap.set(CAP_PROP_FPS, 30);
				c_alto = 480;
				c_ancho = 640;
				BanderaResolucion = false;
			}
		}

		// 1.1 Lectura del Frame.
		double tic = (double)cvGetTickCount();
		bool bSuccess = cap.read(*frame); // read a new frame from video
		if (!bSuccess)
		{
			*strMensajes = string("Cannot read a frame from video stream");
			break;
		}

		// 2. Vertical y Horizontal FLIP
		// Tal vez coonvenga, por readabilidad, ponerlas como funciones.
		if (FLIPMOD)
		{
			if (*FlipHead && *FlipMirror)
			{
				varFlip = -1;
				Flipar = true;
			}

			if (*FlipHead && (!*FlipMirror))
			{
				varFlip = 0;
				Flipar = true;
			}

			if ((!*FlipHead) && *FlipMirror)
			{
				varFlip = 1;
				Flipar = true;
			}

			if ((!*FlipHead) && (!*FlipMirror))
			{
				varFlip = 0;
				Flipar = false;
			}


			FLIPMOD = false;
		}

		if (Flipar)
			flip(*frame, *frame, varFlip);

		//3. Conversion a Espacios y Crear "Combinado" -> Img con el obj filtrado.
		cvtColor(*frame, HSV_image, cv::COLOR_BGR2HSV); //BGR to HSV
		cvtColor(*frame, LAB_image, cv::COLOR_BGR2Lab); //to LAB.
		inRange(HSV_image, hsvlow, hsvhigh, HSV_imageM);
		inRange(LAB_image, lablow, labhigh, LAB_imageM);
		Mat* pMats[2];
		pMats[0] = &HSV_imageM;
		pMats[1] = &LAB_imageM;
		for (int x = 0; x < 2; x++) {
			erode(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(7, 7)));
			dilate(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
		}

		for (int x = 0; x < 2; x++) {
			dilate(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
			erode(*pMats[x], *pMats[x], getStructuringElement(MORPH_ELLIPSE, cv::Size(5, 5)));
		}
		bitwise_and(*pMats[0], *pMats[1], combinado);

		// Seguir el objeto de forma natural
		if (trackObject) {
			int ch[] = { 0, 0 };
			hue.create(HSV_image.size(), HSV_image.depth()); // matriz similar a HSV.
			mixChannels(&HSV_image, 1, &hue, 1, ch, 1); // llenar HUE

														// Seguir a un NUEVO objeto.
			if (trackObject < 0)
			{
				if (SQ) {
					Mat	roi(hue, rec), maskroi(combinado, rec); // solo regiones de interes en color y en BIN.
					trackWindow = rec; // es un Rect.
					calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges); // hitograma de HUE
				}
				else {
					if (MascaraELIP.x < 0) {
						*strMensajes = std::string("Objeto no detectado");
						trackObject = 0;
						continue;
					}
					if (MascaraELIP.x < 1)
						MascaraELIP.x = 0;
					if (MascaraELIP.y < 1)
						MascaraELIP.y = 0;
					if (MascaraELIP.width + MascaraELIP.x >= frame->cols)
						MascaraELIP.width = frame->cols - MascaraELIP.x;
					if (MascaraELIP.height + MascaraELIP.y >= frame->rows)
						MascaraELIP.height = frame->rows - MascaraELIP.y;
				
					Mat roi(hue, MascaraELIP);
					Mat maskroi(combinado, MascaraELIP); // solo regiones de interes en color y en BIN.
					trackWindow = MascaraELIP;
					calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges); // histograma de HUE
				}

				normalize(hist, hist, 0, 255, NORM_MINMAX); // normalizar.
				trackObject = 1;


				histimg = Scalar::all(0);
				int binW = histimg.cols / hsize;
				Mat buf(1, hsize, CV_8UC3);
				for (int i = 0; i < hsize; i++)
					buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180. / hsize), 255, 255);

				cvtColor(buf, buf, COLOR_HSV2BGR);

				for (int i = 0; i < hsize; i++)
				{
					int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows / 255);
					rectangle(histimg, cv::Point(i*binW, histimg.rows),
						cv::Point((i + 1)*binW, histimg.rows - val),
						Scalar(buf.at<Vec3b>(i)), -1, 8);
				}
			}


			// Dibujar Objetos Encontrados
			Point2f centroE;
			//float radioE;
			Mat auxiliarTMP = combinado.clone();
			vector<vector<cv::Point> > contornos;
			vector<Vec4i> hierarchy;
			findContours(auxiliarTMP, contornos, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			if (contornos.size() > 0) {
				bool banBigBlobDetected = false;
				for (int i = 0; i < contornos.size(); i++) {
					// Suele dibujarse
					if (contornos[i].size() > 20) {
						drawContours(*frame, contornos, i, Scalar(0, 255, 255), 2);
						banBigBlobDetected = true;
						RotatedRect tmpElip = fitEllipse(contornos[i]);
						Point2f CoordsBlob = tmpElip.center;

						tmp_X_axis = CoordsBlob.x;
						tmp_Y_axis = CoordsBlob.y;
						Size2f temporal = tmpElip.size; //el height.
						tmp_Z_axis = temporal.height;
					}
					else {
						drawContours(*frame, contornos, i, Scalar(255, 255, 0), 1);
					}

				}
				if (banBigBlobDetected) {
					// HACER ALGO AQUI?
					// QUE SE HACE AQUI?
					// NO SE!
				}
				
			}
			
			calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
			backproj &= combinado; // muy importante!!
			int64 npixels = countNonZero(backproj);
			double porcentaje = ((double)npixels / (double)totpixels);
			// porcentaje had the intention of using a ratio, instead of absolute value.
			if (npixels > 15) {
				RotatedRect trackBox = CamShift(backproj, trackWindow, TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1));
				ellipse(*frame, trackBox, Scalar(255, 128, 0), 3, LINE_AA);
				rectangle(*frame, trackBox.boundingRect(), Scalar(0, 224, 64), 3, LINE_AA);
				X_axis = trackBox.center.x;
				Y_axis = trackBox.center.y;
				Z_axis = trackBox.size.height;
			}
			else {
				*strMensajes = string("he perdido momentaneamente el objeto!");
				X_axis = -1;
				Y_axis = -1;
				Z_axis = -1;
			}

		}

		// Calculo de FPS:
		double toc = (cvGetTickCount() - tic) / cvGetTickFrequency();
		toc = toc / 1000000;
		float fps = (float)(1 / toc);


		if (Seguimiento) {
			// rec = selectROI("edges", frame);
			rec = ELIP.boundingRect();
			Mat frame2 = Mat::zeros(frame->size(), CV_8UC1);
			
			ellipse(frame2, ELIP, Scalar(255, 255, 255), -1, 7); // se perder� la elipse.
			//RotatedRect rec2 = ELIP.boundingRect;
			
			if (!(rec.width == 0 || rec.height == 0))
			{
				vector<Mat> canalesHSV(3);
				vector<Mat> canalesLAB(3);
				Mat subW = (*frame).clone();
				subW = subW(rec);
				Mat subMask = frame2.clone();
				subMask = subMask(rec);
				cvtColor(subW, HSV_crop, CV_BGR2HSV);
				cvtColor(subW, LAB_crop, CV_BGR2Lab);
				split(HSV_crop, canalesHSV);
				split(LAB_crop, canalesLAB);
				Mat LAB_l, LAB_a, LAB_b, HSV_h, HSV_s, HSV_v;
				int Ban = 0;
				//double mL, xL, mA, xA, mB, xB, mH, xH, mS, xS, mV, xV;
				uint8_t mxL, mxA, mxB, mxH, mxS, mxV;
				uint8_t mnL, mnA, mnB, mnH, mnS, mnV;
				mxL = 1; mxA = 1; mxB = 1; mxH = 1; mxS=1; mxV = 1;
				mnL = 255; mnA = 255; mnB = 255; mnH = 255; mnS = 255; mnV=255;
				double vL, vA, vB, vH, vS, vV;
				for (int xx = 0; xx < rec.width; xx++)
					for (int yy = 0; yy < rec.height; yy++) {
						if (frame2.at<uint8_t>(xx, yy) > 0) {
							Ban = 1;
							vL = canalesLAB[0].at<uint8_t>(xx, yy);
							vA = canalesLAB[1].at<uint8_t>(xx, yy);
							vB = canalesLAB[2].at<uint8_t>(xx, yy);
							vH = canalesHSV[0].at<uint8_t>(xx, yy);
							vS = canalesHSV[1].at<uint8_t>(xx, yy);
							vV = canalesHSV[2].at<uint8_t>(xx, yy);
							if (vL > mxL)
								mxL = vL;
							if (vA > mxA)
								mxA = vA;
							if (vB > mxB)
								mxB = vB;
							if (vH > mxH)
								mxH = vH;
							if (vS > mxS)
								mxS = vS;
							if (vV > mxV)
								mxV = vV;
							
							if (vL < mnL)
								mnL = vL;
							if (vA < mnA)
								mnA = vA;
							if (vB < mnB)
								mnB = vB;
							if (vH < mnH)
								mnH = vH;
							if (vS < mnS)
								mnS = vS;
							if (vV < mnV)
								mnV = vV;

						}
					}

				if (mnS > 20) {
					hsvlow = Scalar((uint) mnH, (uint)mnS, (uint)mnV);
				}
				else {
					hsvlow = Scalar((uint)mnH, 20, (uint)mnV);
				}
				if (mxV < 230) {
					hsvhigh = Scalar((uint)mxH, (uint)mxS, (uint)mxV);
				}
				else {
					hsvhigh = Scalar((uint)mxH, (uint)mxS, 230);
				}

				strCola = System::String::Concat("Dat: ", mnH.ToString(), " -:Ban: ", Ban.ToString());
				lablow = Scalar(mnL, mnA, mnB);
				labhigh = Scalar(mxL, mxA, mxB);
				trackObject = -1;
				Seguimiento = false;
			}

		}
		else if (Ban_Deteccion) {
			src = (*frame).clone();
			Mat src_gray;
			Mat src_hsv;
			Mat src_lab;
			Mat out_hsv_low;
			Mat out_hsv_upp;
			Mat out_hsv_Weight;
			Mat dst_rgb;
			Mat src_gray2;
			Mat sharped;
			Mat imgTmp;
			/// Convert it to gray
			cvtColor(src, src_gray, CV_BGR2GRAY);
			cvtColor(src, src_hsv, CV_BGR2HSV);
			cvtColor(src, src_lab, CV_BGR2Lab);
			MascaraELIP.x = -1;
			MascaraELIP.y = -1;
			Mat ch1, ch2, ch3;
			vector<Mat> channels(3);
			split(src, channels);
			ch1 = channels[0];
			ch2 = channels[1];
			ch3 = channels[2];
			vector<Mat> hsvChannels(3);
			vector<Mat> labChannels(3);
			split(src_hsv, hsvChannels);
			split(src_lab, labChannels);
			Mat hueX;
			int chX[] = { 0, 0 };
			int chC[] = { 0,0,1,1,1,2 };


			bool flagElipse;
			switch (bola) {
			case 1:
				// ROJO
				inRange(src_hsv, Scalar(0, 100, 0), Scalar(15, 255, 220), out_hsv_low);
				inRange(src_hsv, Scalar(160, 100, 0), Scalar(179, 255, 220), out_hsv_upp);
				addWeighted(out_hsv_low, 1.0, out_hsv_upp, 1.0, 0.0, out_hsv_Weight, -1);
				break;
			case 2:
				// NARANJA-AMARILLO
				inRange(src_hsv, Scalar(16, 100, 0), Scalar(35, 255, 255), out_hsv_Weight);
				break;
			case 3:
				// VERDE
				inRange(src_hsv, Scalar(35, 60, 0), Scalar(85, 255, 220), out_hsv_Weight);
				break;
			case 4:
				//CYAN
				inRange(src_hsv, Scalar(86, 100, 0), Scalar(105, 255, 255), out_hsv_Weight);
				break;
			case 5:
				//AZUL
				inRange(src_hsv, Scalar(106, 100, 0), Scalar(135, 255, 255), out_hsv_Weight);
				break;
			case 6:
				//VIOLETA
				inRange(src_hsv, Scalar(136, 100, 0), Scalar(159, 255, 255), out_hsv_Weight);
				break;
			default:
				*strMensajes = string("N�mero no v�lido");
				break;
			}


			GaussianBlur(src_gray, imgTmp, cv::Size(0, 0), 3);
			addWeighted(src_gray, 1.5, imgTmp, -0.5, 0, sharped);
			Mat MascaraBolaDetect;
			Mat auxTMP = out_hsv_Weight.clone();
			vector<vector<cv::Point> > contornos;
			RotatedRect ELIP;
			firstobject = 0;
			findContours(auxTMP, contornos, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			if (contornos.size() > 0) {
				for (int i = 0; i < contornos.size(); i++) {
					double area0 = contourArea(contornos[i]);
					if (area0 > 100.0) {
						//cout << "Area de Objeto " << i << " es: " << (double)area0 << endl;
						drawContours(src, contornos, i, Scalar(0, 255, 255), 2);
						ELIP = fitEllipse(contornos[i]);
						if (firstobject == 0) {
							drawContours(MascaraBolaDetect, contornos, i, Scalar(255, 255, 255), -1);
							MascaraELIP = ELIP.boundingRect();
							// Solo memoriza el primer objeto encontrado mayor a 100 unidades.
							firstobject++;
						}
						ellipse(*frame, ELIP, Scalar(100, 00, 240), 2, 7); // se perder� la elipse.
						flagElipse = true;
						// SE DEBE APLICAR TOMA DE HSV para INCREMENTAR LA REGION.
					}
				}
			}

			if (flagElipse) {
				Mat frame2 = Mat::zeros(src.size(), CV_8UC1); //es UNA banda.
				ellipse(frame2, ELIP, Scalar(255, 255, 255), -1, 8);

				/// Establish the number of bins
				int histSize = 256;
				//cout << "Hola-Uso de MAT3" << endl;
				/// Set the ranges ( for B,G,R) )
				float range[] = { 0, 256 };
				const float* histRange = { range };
				bool uniform = true;
				bool accumulate = false;

				Mat b_hist, g_hist, r_hist;
				Mat H_hist, S_hist, V_hist;
				/// Compute the histograms:
				Scalar mediaL, mediaA, mediaB, desvL, desvA, desvB;
				Scalar mediaH, mediaS, mediaV, desvH, desvS, desvV;
				meanStdDev(labChannels[0], mediaL, desvL, frame2);
				meanStdDev(labChannels[1], mediaA, desvA, frame2);
				meanStdDev(labChannels[2], mediaB, desvB, frame2);
				meanStdDev(hsvChannels[0], mediaH, desvH, frame2);
				meanStdDev(hsvChannels[1], mediaS, desvS, frame2);
				meanStdDev(hsvChannels[2], mediaV, desvV, frame2);
				calcHist(&channels[0], 1, 0, frame2, b_hist, 1, &histSize, &histRange, uniform, accumulate);
				calcHist(&channels[1], 1, 0, frame2, g_hist, 1, &histSize, &histRange, uniform, accumulate);
				calcHist(&channels[2], 1, 0, frame2, r_hist, 1, &histSize, &histRange, uniform, accumulate);

				calcHist(&hsvChannels[0], 1, 0, frame2, H_hist, 1, &histSize, &histRange, uniform, accumulate);
				calcHist(&hsvChannels[1], 1, 0, frame2, S_hist, 1, &histSize, &histRange, uniform, accumulate);
				calcHist(&hsvChannels[2], 1, 0, frame2, V_hist, 1, &histSize, &histRange, uniform, accumulate);
				
				double mH, mS, mV, xH, xS, xV;
				double mL, mA, mB, xL, xA, xB;
				mH = mediaH[0] - 3 * desvH[0];
				mS = mediaS[0] - 2 * desvS[0];
				mV = mediaV[0] - 2 * desvV[0];
				xH = mediaH[0] + 3 * desvH[0];
				xS = mediaS[0] + 2 * desvS[0];
				xV = mediaV[0] + 2 * desvV[0];

				mL = mediaL[0] - 2 * desvL[0];
				mA = mediaA[0] - 2 * desvA[0];
				mB = mediaB[0] - 2 * desvB[0];
				xL = mediaL[0] + 2 * desvL[0];
				xA = mediaA[0] + 2 * desvA[0];
				xB = mediaB[0] + 2 * desvB[0];

				double* pV[12];
				pV[0] = &mH; pV[1] = &mS;
				pV[2] = &mV; pV[3] = &xH;
				pV[4] = &xS; pV[5] = &xV;
				pV[6] = &mL; pV[7] = &mA;
				pV[8] = &mB; pV[9] = &xL;
				pV[10] = &xA; pV[11] = &xB;
				for (int j = 0; j < 12; j++) {
					if (*pV[j] < 0)
						*pV[j] = 0;

					if (*pV[j] > 255)
						*pV[j] = 255;
				}

				if (mS > 20) {
					hsvlow = Scalar(mH, mS, mV);
				}
				else {
					hsvlow = Scalar(mH, 20, mV);
				}
				if (xV < 230) {
					hsvhigh = Scalar(xH, xS, xV);
				}
				else {
					hsvhigh = Scalar(xH, xS, 230);
				}


				lablow = Scalar(mL, mA, mB);
				labhigh = Scalar(xL, xA, xB);
				SQ = false;

				trackObject = -1; // SIGNIFICA que SI puedo lanzar el tracker!
				SQ = false;


			}
			Ban_Deteccion = false;
			//Seguimiento = true;

		}
		else if (Verbose == true)
		{
			//const char C1 = "HSV";
			//const char C2 = "LAB";
			//label2->Text = "HSV: ";
			//label3->Text = "LAB: ";
			for (int loop = 0; loop < 1; loop++)
			{
				//label2->Text = System::String::Concat(hsvlow[loop].ToString(), "-", hsvhigh[loop].ToString(), ", ");
				//label3->Text = System::String::Concat(lablow[loop].ToString(), "-", labhigh[loop].ToString(), ", ");
			}

			Verbose = false;
		}
		normXX = (X_axis / c_ancho) * 255;
		normYY = (Y_axis / c_alto) * 255;
		normZZ = (Z_axis / c_alto) * 255;
		char bufercito[60];
		snprintf(bufercito, sizeof(bufercito), "FPS: %.2f, X: %.1f, Y: %.1f, Z: %.1f", fps, X_axis, Y_axis, Z_axis);
		std::string buferSTR = bufercito;

		Mat Formas(frame->rows, frame->cols, CV_8UC3);
		Formas.setTo(Scalar(0, 0, 0));
		Mat Salida;
		cv::Point F1, F2;
		cv::Rect RR1;
		RR1.x = 5; // (int) 0.1 * frame.cols;
		RR1.y = 220; // F1.y = (int) 0.5 * frame.rows; // CAMBIADO
		RR1.width = 210; // (int) 0.6 * frame.cols;
		RR1.height = 15; // 0.21 * frame.rows; //CAMBIADO
		F1.x = 20;  //100; // (int) 0.1 * frame.cols;
		F1.y = 20; // (int) 0.9 * frame->rows;
		F2.x = 250; //(int) 0.2 * frame.cols;
		F2.y = 225;//(int) 0.2 * frame.rows;
				   //if (waitKey(1) == 32) {
		cv::rectangle(Formas, RR1, Scalar(32, 224, 32), 1, 8, 0);
		//}
		cv::putText(Formas, buferSTR, F1, FONT_HERSHEY_COMPLEX_SMALL, 0.6, Scalar(224, 192, 32));
		//cv::putText(Formas, bufercito, F2, FONT_HERSHEY_COMPLEX_SMALL, 0.5, Scalar(224, 224, 32));
		addWeighted(*frame, 1, Formas, 0.5, 0.0, *frame);
		*frameDRAW = frame->clone();
		Banderita = 1;
		//DrawCVImage(this->panel1, frame.clone());//DrawCVImage(this->panel1, frame.clone());
		//DrawCVImage(this->panel1, frame.clone());
		//int x;
		// EJEMPLO DE TEXTO

	}
	cap.release();
	return;

}