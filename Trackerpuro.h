#ifndef _TRACKERPURO_
#define _TRACKERPURO_

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/tracking.hpp>
#include <opencv\cv.hpp>
#include "pluginapi.h"
#include "Shared_Memory.h"
//using namespace cv;
//using namespace std;
 
#pragma once
ref class Trackerpuro
{
public:
	Trackerpuro();
	~Trackerpuro();
	void Detectar(void);
	int ExecDetect(bool Valor);
	int Fun2Mirror(bool Valor);
	int Fun2Invert(bool Valor);
public:
	// Tal vez estos se conviertan en punteros, para facilitar la comunicación.
	bool SALIR;
	bool BanderaResolucion;
	int c_alto, c_ancho;
	bool res320x240, res640x480;
	int FLIPMOD, varFlip;
	bool Flipar;
	bool* FlipHead;
	bool* FlipMirror;
	int trackObject;
	bool Seguimiento;
	bool Ban_Deteccion;
	bool Verbose;
	int bola; // representa el color de la bola a seguir.
	cv::Mat* frame;
	cv::Mat* frameDRAW;
	int Banderita;
	

	//int trackObject = 0;

	// This variables will be shared:
	double X_axis, Y_axis, Z_axis;
	double tmp_X_axis;
	double tmp_Y_axis;
	double tmp_Z_axis;
	double *RShoulder_X; // USADO?
	double normXX, normYY, normZZ;
	System::String^ strCola;
private:
	std::string* strMensajes;
};

#endif