#pragma once
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/tracking.hpp>
#include <opencv\cv.hpp>
#include "OptAvanz.h"
#include "Trackerpuro.h"


namespace WinTemp1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace cv;
	using namespace std;
	using namespace System::Threading;

	/// <summary>
	/// Es la Clase que implementa el tracking
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		//Thread^ hiloU;
		OptAvanz^ pOpcAvz;
		Mat* Dibujado;
		//Bandera;
		WinTemp1::OptAvanz^ WopcAvz; //= gcnew WinTemp1::OptAvanz();
		Trackerpuro^ HiloTrack; // = gcnew Trackerpuro();
	private: System::Windows::Forms::Label^  label1;
	public:
		Thread^ hiloU; // = gcnew Thread(gcnew ThreadStart(HiloTrack, &Trackerpuro::Detectar));

		MyForm()
		{
			InitializeComponent();
			//TODO: agregar c�digo de constructor aqu�
			BanderaResolucion = false;
			Verbose           = false;
			Ban_Deteccion     = false;
			FLIPMOD           = true;
			Seguimiento       = false;
			hiloActivado      = false;
			Run1raVez         = true;
			Flipar = 0;
			SALIR = 0;
			bola = 3;
			varFlip = 0;
			c_alto = 240;
			c_ancho = 320;
			XX = 0;
			YY = 0;
			ZZ = 0;
//			WopcAvz = gcnew WinTemp1::OptAvanz(); // CASCADA!
			HiloTrack = gcnew Trackerpuro();
			hiloU = gcnew Thread(gcnew ThreadStart(HiloTrack, &Trackerpuro::Detectar));
			
			this->WopcAvz = (gcnew WinTemp1::OptAvanz());
			WopcAvz->LigarHilo(HiloTrack);
			//hiloU = HILO;
			//
			//this->OpcAvz = (gcnew WinTemp1::OptAvanz());
			// conexiones de Ventanas:
		}
		void cerrarCam();
		
		
	private: 
		System::Windows::Forms::Button^ button5;
		System::Windows::Forms::Button^ button6;
		System::Windows::Forms::Panel^  panel1;

		System::Windows::Forms::Button^ button2;
		System::Windows::Forms::Button^ button3;

		System::Windows::Forms::GroupBox^  groupBox2;
		System::Windows::Forms::RadioButton^  radioButton5;
		System::Windows::Forms::RadioButton^  radioButton4;
		System::Windows::Forms::RadioButton^  radioButton3;
		System::Windows::Forms::RadioButton^  radioButton7;
		System::Windows::Forms::RadioButton^  radioButton6;

protected:
	/// <summary>
	/// Limpiar los recursos que se est�n usando.
	/// </summary>
	//public:
	bool BanderaResolucion;
	bool Verbose;
	bool Ban_Deteccion;
	bool FLIPMOD;
	bool Seguimiento;
	bool hiloActivado;
	bool Run1raVez;
	double c_alto;
	double c_ancho;
	double XX;
	double YY;
	int SALIR;
	double ZZ;
	//OptAvanz ^ cosaX;
	int bola;
	int Flipar;
	private: System::Windows::Forms::Timer^  timer1;
	protected:

protected:
	int varFlip;
	
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>

		int trackObject = 0;
		double Pressure;
		double X_axis;
		double Y_axis;
		double Z_axis;
		

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton7 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Location = System::Drawing::Point(-4, 3);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(640, 480);
			this->panel1->TabIndex = 0;
			this->panel1->Click += gcnew System::EventHandler(this, &MyForm::panel1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(416, 606);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(139, 23);
			this->button2->TabIndex = 5;
			this->button2->Text = L"Start Video";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Visible = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(416, 487);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(220, 23);
			this->button3->TabIndex = 6;
			this->button3->Text = L"Detectar Esfera";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(416, 516);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(220, 23);
			this->button5->TabIndex = 10;
			this->button5->Text = L"Opciones Avanzadas";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(561, 549);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 13;
			this->button6->Text = L"Verbose";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Visible = false;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->radioButton7);
			this->groupBox2->Controls->Add(this->radioButton6);
			this->groupBox2->Controls->Add(this->radioButton5);
			this->groupBox2->Controls->Add(this->radioButton4);
			this->groupBox2->Controls->Add(this->radioButton3);
			this->groupBox2->Location = System::Drawing::Point(241, 489);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(169, 135);
			this->groupBox2->TabIndex = 15;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Detecci�n de Esfera";
			// 
			// radioButton7
			// 
			this->radioButton7->AutoSize = true;
			this->radioButton7->Location = System::Drawing::Point(7, 112);
			this->radioButton7->Name = L"radioButton7";
			this->radioButton7->Size = System::Drawing::Size(156, 17);
			this->radioButton7->TabIndex = 4;
			this->radioButton7->TabStop = true;
			this->radioButton7->Text = L"Detectar otro color (manual)";
			this->radioButton7->UseVisualStyleBackColor = true;
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Checked = true;
			this->radioButton6->Location = System::Drawing::Point(7, 20);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(130, 17);
			this->radioButton6->TabIndex = 3;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"Detecci�n Autom�tica";
			this->radioButton6->UseVisualStyleBackColor = true;
			this->radioButton6->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton6_CheckedChanged);
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(7, 89);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(128, 17);
			this->radioButton5->TabIndex = 2;
			this->radioButton5->Text = L"Detectar esfera verde";
			this->radioButton5->UseVisualStyleBackColor = true;
			this->radioButton5->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton5_CheckedChanged);
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Location = System::Drawing::Point(7, 67);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(118, 17);
			this->radioButton4->TabIndex = 1;
			this->radioButton4->Text = L"Detectar esfera roja";
			this->radioButton4->UseVisualStyleBackColor = true;
			this->radioButton4->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton4_CheckedChanged);
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(7, 43);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(120, 17);
			this->radioButton3->TabIndex = 0;
			this->radioButton3->Text = L"Detectar esfera azul";
			this->radioButton3->UseVisualStyleBackColor = true;
			this->radioButton3->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton3_CheckedChanged);
			// 
			// timer1
			// 
			this->timer1->Interval = 25;
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 516);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(29, 13);
			this->label1->TabIndex = 16;
			this->label1->Text = L"Auto";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(636, 633);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->panel1);
			this->Name = L"MyForm";
			this->Text = L"Seguimiento de Esfera";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MyForm::Cerrador);
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		// EMPIEZAN LOS METODOS:


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	}

	public: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		
		return;
	
}

	void DrawCVImage(System::Windows::Forms::Control^ control, cv::Mat& colorImage)
	{
		System::Drawing::Graphics^ graphics = control->CreateGraphics();
		System::IntPtr ptr(colorImage.ptr());
		System::Drawing::Bitmap^ b = gcnew System::Drawing::Bitmap(colorImage.cols, colorImage.rows, colorImage.step, System::Drawing::Imaging::PixelFormat::Format24bppRgb, ptr);
		System::Drawing::RectangleF rect(0, 0, control->Width, control->Height);
		graphics->DrawImage(b, rect);
		delete graphics;
	}



private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	BanderaResolucion = true;
}
private: System::Void radioButton2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	BanderaResolucion = true;
}
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	//Thread^ hiloU = gcnew Thread(gcnew ThreadStart(MyForm, &Detectar));
	if (hiloActivado) {
		//hiloU->Abort();
		hiloU->Suspend();
		Run1raVez = false;
		hiloActivado = false;
		button2->Text = "Start Video";
	}
	else {
		if (Run1raVez) {
			hiloU->Start();
			//Sleep(1000);
			this->timer1->Start();
		}
		else {
			hiloU->Resume();
		}
		hiloActivado = true;
		button2->Text = "Stop Video";
	}

	return;
}

		 private: System::Void MyForm_Close(System::Object^  sender, System::EventArgs^  e) {
			 //System::Windows::Forms::MessageBox::Show("ADIOS2","Tit2",MB_OK);
			 MessageBox::Show("Cosa");
			 
			 cerrarCam();
			 hiloU->Abort();
		 }
private: System::Void MyForm_Unload(System::Object^  sender, System::EventArgs^  e) {
	MessageBox::Show("Cosa2222");
	cerrarCam();
	hiloU->Abort();
	this->timer1->Stop();

}
		 
private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	this->Size.Height = 520;
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	Verbose = false;
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	//Ban_Deteccion = true;
	//(HiloTrack->Ban_Deteccion) = true;
	HiloTrack->ExecDetect(true);

}
private: System::Void checkBox2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	//FLIPMOD = true;
}
private: System::Void checkBox3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	//FLIPMOD = true;
}
private: System::Void radioButton5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	HiloTrack->bola = 3;
}
private: System::Void radioButton4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		HiloTrack->bola = 1;
}
private: System::Void radioButton3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	HiloTrack->bola = 5;
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	//pOpcAvz->Show();
	// Ventana de Opciones Avanzadas.
	//WinTemp1::MyForm^ form = gcnew WinTemp1::MyForm();
	WopcAvz->Show();
	
	//this->pOpcAvz->Show();
	
	//Application::Run();
}
		 
private: System::Void panel1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (this->Height > 650) {
				 this->Height = 521;
			 }
			 else {
				 this->Height = 670;

			 }
		 }

private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	
}
private: System::Void radioButton6_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	HiloTrack->bola = 3;
}
private: System::Void Cerrador(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	cerrarCam();
	hiloU->Abort();
}
private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	if (HiloTrack->Banderita) {
		DrawCVImage(this->panel1, *(HiloTrack->frameDRAW));
		label1->Text = HiloTrack->strCola;
	}
}
};


void WinTemp1::MyForm::cerrarCam(void) {
	SALIR = 1;
}

}
